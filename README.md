# README

<h2>Routes</h2>

-**POST /v1/users**

Создание пользователя - body: { name: 'username', points: '100' }
        
-**GET /v1/users**

Список всех пользователей с балансами

-**POST /v1/transactions**

Транзакция на изменение баланса - body: { name: 'username', amount: -100 }

-**GET  /v1/transactions?name=username**

Список всех транзакций пользователя

<h2>Тесты</h2>

- rspec
- apache benachmark (см файл test.sh)
