class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name, unique: true
      t.integer :points, default: 0
      t.timestamps
      t.index :name, unique: true
    end
  end
end
