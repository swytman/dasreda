class NewTransaction
  attr_reader :name, :amount, :result, :error

  def initialize(data)
    @name = data[:name]
    @amount = data[:amount]
    @error = false
    @result = false
  end

  def perform
    raise 'Zero amount' if amount.zero?
    user = User.find_by(name: name)
    raise "User '#{name}' does not exit" if user.nil?
    raise 'Not enough points' if user.points + amount < 0

    user.with_lock do
      user.points += amount
      user.save!
    end

    @result = user.transactions.create(amount: amount)
    self
  rescue StandardError => e
    Rails.logger.error(e)
    @error = e
    self
  end

  def success?
    !error
  end
end