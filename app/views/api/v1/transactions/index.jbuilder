json.array! @transactions do |transaction|
  json.name transaction.user.name
  json.amount transaction.amount
  json.time I18n.l(transaction.created_at)
end