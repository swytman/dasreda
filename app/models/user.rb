class User < ApplicationRecord
  has_many :transactions, dependent: :delete_all

  validates :name, presence: true, uniqueness: true
end
