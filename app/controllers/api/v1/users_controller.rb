module Api
  module V1
    class UsersController < ApplicationController
      before_action :set_user, only: [:show]

      # GET /users
      def index
        @users = User.all
        render :index, status: :ok
      end

      # POST /users
      def create
        @user = User.new(user_params)
        @user.save!
        render :create, status: :created
      rescue
        render json: { error: 'Some error' }, status: :unprocessable_entity
      end

      private

      def set_user
        @user = User.find(params[:id])
      end

      def user_params
        params.permit(:name, :points)
      end
    end
  end
end
