module Api
  module V1
    class TransactionsController < ApplicationController

      # GET /transactions
      def index
        name = params.permit(:name)[:name]
        user = User.find_by(name: name)

        if user
          @transactions = user.transactions.order('id DESC')
          render :index, status: :ok
        else
          render json: { error: 'User not found' }, status: :unprocessable_entity
        end
      end

      # POST /transactions
      def create
        @transaction = NewTransaction.new(transaction_params).perform
        if @transaction.success?
          @transaction = @transaction.result
          render :create, status: :ok
        else
          render json: { error: @transaction.error }, status: :unprocessable_entity
        end
      end

      private

      def transaction_params
        params.permit(:name, :amount)
      end
    end
  end
end
