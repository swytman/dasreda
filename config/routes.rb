Rails.application.routes.draw do
  scope module: 'api' do
    get '/400', to: 'errors#bad_request'
    get '/404', to: 'errors#not_found'
    get '/500', to: 'errors#internal_server_error'
    namespace :v1 do
      resources :users, only: [:index, :create]
      resources :transactions, only: [:index, :create]
    end
  end
end
