require 'spec_helper'

describe 'test NewTransaction concurrency' do

  it 'correct simultaneous transactions' do
    User.create(name: 'Vasya')
    NewTransaction.new({ name: 'Vasya', amount: 100 }).perform
    threads = 10.times.map do |i|
      Thread.new do
        amount = i.even? ? 100 : -100
        NewTransaction.new({ name: 'Vasya', amount: amount }).perform
      end
    end
    threads.each(&:join)
    expect(User.find_by(name: 'Vasya').points).to eq(100)
  end

end