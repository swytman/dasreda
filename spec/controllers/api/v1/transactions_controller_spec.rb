require 'spec_helper'

describe Api::V1::TransactionsController do
  before (:each) do
    request.headers['Content-Type'] = 'application/json'
  end
  describe "GET #index" do
    it 'should return user transactions' do
      User.create(name: 'Vasya')
      10.times { NewTransaction.new({ name: 'Vasya', amount: 100 }).perform }
      get :index, params: { name: 'Vasya', format: :json }
      expect(response.code).to eq('200')
    end

    it 'should return error when user does not exist' do
      get :index, params: { name: 'Vasya', format: :json }
      expect(response.code).to eq('422')
    end
  end

  describe "#create" do
    before (:each) do
      User.create(name: 'Vova')
    end

    it 'should change user amount' do
      post :create, params: { name: 'Vova', amount: 300 }
      expect(response.code).to eq('200')
      expect(User.find_by(name: 'Vova').points).to eq(300)
    end

    it 'balance can not be negative' do
      post :create, params: { name: 'Vova', amount: -300 }
      expect(response.code).to eq('422')
    end
  end
end