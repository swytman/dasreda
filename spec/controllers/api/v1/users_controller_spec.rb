require 'spec_helper'

describe Api::V1::UsersController do
  before (:each) do
    request.headers['Content-Type'] = 'application/json'
  end
  describe "#index" do
    before (:each) do
      create_list(:user, 20)
      request.headers['Content-Type'] = 'application/json'
    end
    it 'has 20 users with only two attributes - attributes name and points' do
      get :index
      expect(response.code).to eq('200')
      users = JSON.parse(response.body)
      expect(users.length).to eq(20)
      users.each do |user|
        expect(user).to have_key('name')
        expect(user).to have_key('points')
        expect(user.keys.length).to eq(2)
      end
    end
  end

  describe "#create" do
    it 'should create user' do
      post :create, params: { name: 'Vovan', points: '400' }
      expect(User.where(name: 'Vovan')).to exist
    end
    it 'user should have uniq name' do
      User.create({ name: 'Vova' } )
      post :create, params: { name: 'Vova' }
      expect(response.code).to eq('422')
    end
  end
end